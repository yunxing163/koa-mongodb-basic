const Koa = require('koa')
const app = new Koa()
const bodyparser = require('koa-bodyparser');
const cors = require('koa2-cors');
const router = require('./app/routes')

// 处理跨域
app.use(cors());
app.use(async (ctx, next) => {
  ctx.set("Access-Control-Allow-Origin", "*")
  await next()
})
// 中间件可以将post请求的参数转为json格式返回
app.use(bodyparser());

app.use(router.routes())
app.use(router.allowedMethods())

app.listen(3000, () => {
  console.log(`Listening: http://localhost:3000/`)
});
