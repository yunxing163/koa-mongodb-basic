const router = require('koa-router')()

const { users } = require('../controllers')

//用户操作
router.get('/users/list',users.list)
router.post('/users/add',users.addUser)
router.put('/users/edit',users.editUser)
router.delete('/users/delete',users.deleteUser)

module.exports = router
